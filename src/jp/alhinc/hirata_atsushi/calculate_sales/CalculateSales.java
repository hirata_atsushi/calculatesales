package jp.alhinc.hirata_atsushi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void outputFiles(
			String filePath,
			Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		BufferedWriter bufferedWriter = null;
		try {
			File file = new File(filePath);
			bufferedWriter = new BufferedWriter(new FileWriter(file));

			//keySet()メソッドを使ってfor文で回す。
			for (String key : branchNames.keySet()) {
				bufferedWriter.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bufferedWriter.newLine();
			}

		} catch (IOException e) {
			System.out.println("エラーが発生しました。");
			return;

		} finally {
			if (bufferedWriter != null) {
				try {
					bufferedWriter.close();

				} catch (IOException e) {
					System.out.println("closeできませんでした。");
					return;
				}
			}

		}

	}

	public static void inputFile(
			String filePath,
			String fileName,
			String word,
			Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		BufferedReader buffrerdReader = null;
		try {
			//Fileオブジェクトの生成。引数にファイルパスを指定。
			File file = new File(filePath, fileName);
			if (!file.exists()) {
				System.out.println(word + "定義ファイルが存在しません");
				return;
			}
			//Fileオブジェクトを引数として文字列を受け取る。
			FileReader fileReader = new FileReader(file);
			buffrerdReader = new BufferedReader(fileReader);
			String line;

			//brが空になるまでファイルの中身を一行ずつ読み込む
			while ((line = buffrerdReader.readLine()) != null) {
				String[] parsedLines = line.split(",", 0);

				//支店定義ファイルの名前が不正の場合のエラー処理
				if ((parsedLines.length != 2) || (!parsedLines[0].matches("^[0-9]{3}$"))) {
					System.out.println(word + "定義ファイルのフォーマットが不正です");
					return;
				}

				//「支店コードと支店名」、「支店コードと売上」が紐づいたMapにする
				branchNames.put(parsedLines[0], parsedLines[1]);
				branchSales.put(parsedLines[0], 0L);
			}

		} catch (IOException e) {
			System.out.println("エラーが発生しました。");
			return;

		} finally {
			if (buffrerdReader != null) {
				try {
					buffrerdReader.close();

				} catch (IOException e) {
					System.out.println("closeできませんでした。");
					return;
				}
			}
		}

	}

	public static void main(String[] args) {

		//ファイルを閉じることのできる状態をつくるため、空のBufferedReader型の変数を定義
		Map<String, String> branchNames = new HashMap<String, String>();
		Map<String, Long> branchSales = new HashMap<String, Long>();
		List<File> branchSalesLists = new ArrayList<File>();

		//コマンドライン引数が1つだけ渡されているかの確認
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		inputFile(args[0], "branch.lst", "支店", branchNames, branchSales);

		//売上集計システムディレクトリ内にある売上ファイルの配列を生成。
		File[] fileLists = new File(args[0]).listFiles();
		for (int i = 0; i < fileLists.length; i++) {
			//売上ファイル名を抽出
			String fileName = fileLists[i].getName();

			//売上ファイル名のフォーマットを確認。
			if (fileLists[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				//売上ファイルリストに格納
				branchSalesLists.add(fileLists[i]);
			}
		}

		//売上ファイルの名前で昇順ソート
		Collections.sort(branchSalesLists);

		//連番チェック
		for (int i = 0; i < branchSalesLists.size() - 1; i++) {
			//売上ファイルリスト内のパスを文字列として取得しその8文字目までを切り出している
			int former = Integer.parseInt(branchSalesLists.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(branchSalesLists.get(i + 1).getName().substring(0, 8));

			if (latter - former != 1) {
				System.out.println("連番ではありません");
				return;
			}
		}

		//売上ファイルを一つずつ読み込む

		BufferedReader buffrerdReader2 = null;
		for (int j = 0; j < branchSalesLists.size(); j++) {
			try {
				buffrerdReader2 = new BufferedReader(new FileReader(branchSalesLists.get(j)));
				/*
				 * 売上ファイルの中身を一行ずつ格納するためのリスト。
				 * whileの外で宣言しているので、この場合1ファイルにつき必ず2行のリストになる。
				 */
				List<String> readLines = new ArrayList<String>();
				String line = "";
				while ((line = buffrerdReader2.readLine()) != null) {
					//売上ファイルの中身を一行ずつ追加
					readLines.add(line);
				}

				String fileName = branchSalesLists.get(j).getName();
				//支店コードを取得
				String branchCord = readLines.get(0);

				//読み込んだ売上ファイルの中身が2行でない場合のエラー処理
				if (readLines.size() != 2) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				//「売上ファイル記載の支店コード」が「支店定義ファイル記載の支店コード」に該当しない場合のエラー処理
				if (!branchNames.containsKey(branchCord)) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				if (!readLines.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//売上をLong型に変換しつつ取得
				Long branchSale = Long.parseLong(readLines.get(1));

				//支店コードと紐づいた売上に加算
				Long totalSales = branchSale + branchSales.get(branchCord);

				//合計金額が10桁超えてしまった場合のエラー処理
				if (totalSales >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//加算した売上を反映させるためMapを上書き
				branchSales.put(branchCord, totalSales);

			} catch (IOException e) {
				System.out.println("エラーが発生しました。");
				return;

			} finally {
				if (buffrerdReader2 != null) {
					try {
						buffrerdReader2.close();

					} catch (IOException e) {
						System.out.println("closeできませんでした。");
						return;
					}
				}
			}

		}

		outputFiles("C:\\Users\\hirata.atsushi\\Desktop\\売上集計システム\\branch.out", branchNames, branchSales);

	}
}